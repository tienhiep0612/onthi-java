package com.example.Controller;

import com.example.Model.SinhVien;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/")
public class SinhVienServle extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private SinhVienService sinhVienService;
    public void init(){
        sinhVienService = new SinhVienService();
    }
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String rqParam = req.getServletPath();
        switch (rqParam){
            case "/insert":
                showFormThemSinhVien(req, resp);
                break;
            case "/add":
                themSinhVien(req,resp);
                break;
            case "/delete":
                xoaSinhVien(req, resp);
                break;
            case "/edit":
                showFormUpdateSinhVien(req, resp);
                break;
            case "/update":
                capNhatSinhVien(req, resp);
                break;
            default:
                showListSinhVien(req,resp);
                break;
        }
    }
    private void showListSinhVien(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<SinhVien> sinhVienList = sinhVienService.danhsachSV();
        req.setAttribute("listSinhVien" , sinhVienList);
        RequestDispatcher dispatcher = req.getRequestDispatcher("/view/list.jsp");
        dispatcher.forward(req,resp);
    }
    private void showFormThemSinhVien(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/view/them.jsp");
        requestDispatcher.forward(req,resp);
    }
    private void themSinhVien(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String tenSinhVien = req.getParameter("tenSV");
        String lop = req.getParameter("lopSV");
        SinhVien SinhVien = new SinhVien(tenSinhVien,lop);
        sinhVienService.themSV(SinhVien);
        resp.sendRedirect("list");
    }
    private void xoaSinhVien(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Integer id = Integer.parseInt(req.getParameter("maSV"));
        sinhVienService.xoaSV(id);
        resp.sendRedirect("list");
    }
    private void showFormUpdateSinhVien(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer id = Integer.parseInt(req.getParameter("maSV"));
        SinhVien SinhVien = sinhVienService.layIDSV(id);
        if (SinhVien != null){
            req.setAttribute("maSV",id);
            req.setAttribute("sinhVien",SinhVien);
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("/view/capnhat.jsp");
            requestDispatcher.forward(req,resp);
        }
    }
    private void capNhatSinhVien(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Integer id = Integer.parseInt(req.getParameter("maSV"));
        String name = req.getParameter("tenSV");
        String lop = req.getParameter("lopSV");
        SinhVien SinhVien = new SinhVien(id,name,lop);
        sinhVienService.capnhatSV(SinhVien);
        resp.sendRedirect("list");
    }
}
