package com.example.Controller;

import com.example.Model.SinhVien;
import com.example.ConnectionDB.SinhVienConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SinhVienService {
    public static Connection connection;
    private static final String SELECT_ALL_FROM_SV = "select * from sinhvien;";
    private static final String INSERT_SV = "insert into sinhvien(tensv,lopsv) values (?,?);";
    private static final String DELETE_SV = "delete from sinhvien where masv = ?;";
    private static final String UPDATE_SV = "update sinhvien set tensv = ?, lopsv = ? where masv = ?;";
    private static final String SELECTION_SV_BY_IDSV = "select * from sinhvien where masv = ?;";

    public Boolean themSV(SinhVien sinh_vien) {
        try {
            connection = SinhVienConnection.openConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_SV);
            preparedStatement.setString(1, sinh_vien.getTenSV());
            preparedStatement.setString(2, sinh_vien.getLopSV());
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public List<SinhVien> danhsachSV() {
        List<SinhVien> SinhVienList = new ArrayList<>();
        try {
            connection = SinhVienConnection.openConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_FROM_SV);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                SinhVien sinh_vien = new SinhVien();
                sinh_vien.setMaSV(resultSet.getInt("masv"));
                sinh_vien.setTenSV(resultSet.getString("tensv"));
                sinh_vien.setLopSV(resultSet.getString("lopsv"));
                SinhVienList.add(sinh_vien);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return SinhVienList;
    }

    public Boolean xoaSV(Integer id) {
        try {
            connection = SinhVienConnection.openConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_SV);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public SinhVien layIDSV(Integer id) {
        SinhVien sinh_vien = new SinhVien();
        try {
            connection = SinhVienConnection.openConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECTION_SV_BY_IDSV);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                sinh_vien.setMaSV(resultSet.getInt("masv"));
                sinh_vien.setTenSV(resultSet.getString("tensv"));
                sinh_vien.setLopSV(resultSet.getString("lopsv"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sinh_vien;
    }

    public void capnhatSV(SinhVien sinh_vien) {
        try {
            connection = SinhVienConnection.openConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_SV);
            preparedStatement.setString(1, sinh_vien.getTenSV());
            preparedStatement.setString(2, sinh_vien.getLopSV());
            preparedStatement.setInt(3, sinh_vien.getMaSV());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
