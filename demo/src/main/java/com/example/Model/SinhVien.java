package com.example.Model;

public class SinhVien {
    private Integer maSV;
    private String tenSV;
    private String lopSV;

    public SinhVien() {
    }
    public SinhVien(String tenSV, String lopSV) {
        this.tenSV = tenSV;
        this.lopSV = lopSV;
    }
    public SinhVien(Integer maSV, String tenSV, String lopSV) {
        this.maSV = maSV;
        this.tenSV = tenSV;
        this.lopSV = lopSV;
    }

    public Integer getMaSV() {
        return maSV;
    }

    public void setMaSV(Integer maSV) {
        this.maSV = maSV;
    }

    public String getTenSV() {
        return tenSV;
    }

    public void setTenSV(String tenSV) {
        this.tenSV = tenSV;
    }

    public String getLopSV() {
        return lopSV;
    }

    public void setLopSV(String lopSV) {
        this.lopSV = lopSV;
    }
}
