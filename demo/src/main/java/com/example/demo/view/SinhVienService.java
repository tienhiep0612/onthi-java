package com.example.demo.view;

import com.example.demo.connection.ConnectionDB;
import com.example.demo.model.SinhVien;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SinhVienService {
    private static final String SELECT_ALL_LIST_SV = "select * from sinhvien;";
    private static final String INSERT_SV = "insert into sinhvien(tensv,lop) values ( ? , ?)";
    private static final String DELETE_SV = "delete from sinhvien where masv = ?;";
    private static final String UPDATE_SV = "update sinhvien set tensv = ? , lop = ? where masv = ?";
    private static final String SELECT_SV_BY_ID = "select * from sinhvien where masv = ?";
    public Boolean themSinhVien(SinhVien SinhVien){
        Connection connection = null;
        try{
            connection = ConnectionDB.openConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_SV);
            preparedStatement.setString(1,SinhVien.getTenSV());
            preparedStatement.setString(2,SinhVien.getLop());
            preparedStatement.executeUpdate();
            ConnectionDB.closeConnection(connection,preparedStatement,null);
            return true;
        }catch (SQLException e) {
            e.printStackTrace();
            ConnectionDB.closeConnection(connection,null,null);
            return false;
        }
    }
    public List<SinhVien> layDanhSachSinhVien(){
        List<SinhVien> SinhVienList = new ArrayList<>();
    try {
        Connection connection = ConnectionDB.openConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_LIST_SV);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()){
            SinhVien SinhVien = new SinhVien();
            SinhVien.setMaSV(resultSet.getInt("ma_SV"));
            SinhVien.setTenSV(resultSet.getString("ten_SV"));
            SinhVien.setLop(resultSet.getString("lop"));
            SinhVienList.add(SinhVien);
        }
        ConnectionDB.closeConnection(connection,preparedStatement,resultSet);
    }catch (SQLException e ){
        e.printStackTrace();
    }
        return SinhVienList;
    }
    public Boolean xoaSinhVien(Integer id){
        Connection connection = null;
        try {
            connection = ConnectionDB.openConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_SV);
            preparedStatement.setInt(1,id);
            preparedStatement.executeUpdate();
            ConnectionDB.closeConnection(connection,preparedStatement,null);
            return true;
        }
         catch (SQLException e){
             ConnectionDB.closeConnection(connection,null,null);
            e.printStackTrace();
            return false;
        }
    }
    public SinhVien laySinhVienTheoID(Integer id){
        SinhVien SinhVien = new SinhVien();
        try{
            Connection connection = ConnectionDB.openConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_SV_BY_ID);
            preparedStatement.setInt(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                SinhVien.setMaSV(resultSet.getInt("ma_SV"));
                SinhVien.setTenSV(resultSet.getString("ten_SV"));
                SinhVien.setLop(resultSet.getString("lop"));
            }
            ConnectionDB.closeConnection(connection,preparedStatement,resultSet);
        }catch (SQLException e){
            e.printStackTrace();
        }
        return SinhVien;
    }
    public void capNhatSinhVien(SinhVien SinhVien){
        try {
            Connection connection = ConnectionDB.openConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_SV);
            preparedStatement.setString(1,SinhVien.getTenSV());
            preparedStatement.setString(2,SinhVien.getLop());
            preparedStatement.setInt(3,SinhVien.getMaSV());
            preparedStatement.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

}
