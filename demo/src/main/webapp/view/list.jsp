<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
    <title>THƯ VIỆN</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            background-color: #f0f0f0;
        }
        h1 {
            color: #333;
            text-align: center;
            padding: 20px 0;
        }
        table {
            width: 100%;
            margin: 20px 0;
            border-collapse: collapse;
        }
        th, td {
            padding: 10px;
            border: 1px solid #ddd;
            text-align: left;
        }
        th {
            background-color: #333;
            color: white;
        }
        tr:nth-child(even) {
            background-color: #f2f2f2;
        }
        a {
            color: #333;
            text-decoration: none;
            margin: 0 10px;
        }
        .btn {
            display: inline-block;
            padding: 10px 20px;
            color: #fff;
            background-color: #333;
            border: none;
            border-radius: 5px;
            text-align: center;
            cursor: pointer;
        }
        .btn:hover {
            background-color: #444;
        }
    </style>
</head>
<body>
<h1>Quản Lý Sách</h1>
<br>
<a href="<%=request.getContextPath()%>/insert" class="btn">Thêm Sách Mới</a>
<h3>Danh Sách Sách</h3>
<table>
    <thead>
    <tr>
        <th>Mã Sách</th>
        <th>Tên Sách</th>
        <th>Loại Sách</th>
        <th>Chức năng</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="sinhVien" items="${listSinhVien}">
        <tr>
            <td><c:out value="${sinhVien.maSV}"/></td>
            <td><c:out value="${sinhVien.tenSV}"/></td>
            <td><c:out value="${sinhVien.lopSV}"/></td>
            <td>
                <a href="<%=request.getContextPath()%>/edit?maSV=${sinhVien.maSV}">Sửa</a>
                <a href="<%=request.getContextPath()%>/delete?maSV=${sinhVien.maSV}">Xoá</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
</body>
</html>