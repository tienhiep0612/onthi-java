package com.example.demo.model;

public class SinhVien {
    private Integer maSV;
    private String tenSV;
    private String lop;

    public SinhVien() {
    }

    public SinhVien(Integer maSV, String tenSV, String lop) {
        this.maSV = maSV;
        this.tenSV = tenSV;
        this.lop = lop;
    }

    public SinhVien(String tenSV, String lop) {
        this.tenSV = tenSV;
        this.lop = lop;
    }

    public Integer getMaSV() {
        return maSV;
    }

    public void setMaSV(Integer maSV) {
        this.maSV = maSV;
    }

    public String getTenSV() {
        return tenSV;
    }

    public void setTenSV(String tenSV) {
        this.tenSV = tenSV;
    }

    public String getLop() {
        return lop;
    }

    public void setLop(String lop) {
        this.lop = lop;
    }
}
